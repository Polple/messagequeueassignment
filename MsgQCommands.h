//
// Created by Paul.P on 20/12/2019.
//

#define MQA 3 //Defines the meaximum amount of queues
#define MQS 5 //Defines the maximum queue size of each message queue
#define MLN 200 //Defines the maximum size of a message

typedef struct MessQ{//A single message queue

    char message[MQS][MLN];
    char id[50];

}MessQ_t;

typedef struct MsgQs{//An array of message queues

    MessQ_t queue[MQA];

}MsgQs_t;


#ifndef MESSAGEQUEUELIBRARY_MSGQCOMMANDS_H
#define MESSAGEQUEUELIBRARY_MSGQCOMMANDS_H

MsgQs_t* initializeMsgQs(MsgQs_t *mq);
void unloadMsgQs(MsgQs_t mq, MsgQs_t** pointer);
void createQ(MsgQs_t mq, char ident[50]);

#endif //MESSAGEQUEUELIBRARY_MSGQCOMMANDS_H

