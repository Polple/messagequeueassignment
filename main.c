#include <stdio.h>
#include "MsgQCommands.h"
#include <string.h>

#define MMQ 5 //Defines the maximum amount of message queues that can be created

int main() {
    MsgQs_t msq;
    MsgQs_t * msqPtr=initializeMsgQs(&msq);

    strcpy(&msq.queue[0].message[0][0], "yellow pen");
    printf("String[0][0]  %s\n",&msq.queue[0].message[0][0]);


    printf("Initialised   %p\n",initializeMsgQs(&msq));

    unloadMsgQs(msq,&msqPtr);
    printf("Unloaded      %p\n",msqPtr);


    char ide[50]="778";
    createQ(msq, ide);
    printf("The identifier for [0] IS: %s\n",msq.queue[0].id);

}