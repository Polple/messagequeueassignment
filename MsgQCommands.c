//
// Created by Paul.P on 20/12/2019.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MQA 3 //Defines the maximum amount of queues
#define MQS 5 //Defines the maximum queue size of each message queue
#define MLN 200 //Defines the maximum size of a message

int qCounter=0; //helps define the next queue to be added

typedef struct MessQ{//A single message queue

    char message[MQS][MLN];
    char id[50];


}MessQ_t;


typedef struct MsgQs{//An array of message queues

    MessQ_t * queue;

}MsgQs_t;




//MsgQs_t Functions:
//Returns a pointer to a MsgQs_t structure and allocates memory for the message queues
MsgQs_t** initializeMsgQs(MsgQs_t *mq){
    MsgQs_t** mqPtr=&mq;
    mq->queue = (MessQ_t *) malloc(MQA * sizeof(mq->queue));
    return  mqPtr;
}


// relinquishes all resources currently held by a MsgQs_t and sets its pointer value to NULL
void unloadMsgQs(MsgQs_t mq, MsgQs_t** pointer){
    *pointer=NULL;
    //Free Resources
}


//creates a new queue and which is assigned an identifier passed as argument
void createQ(MsgQs_t mq, char ident[50]){
    int taken=0;
    for(int i=0;i<MQA;i++){
        if(strcmp(mq.queue[i].id,ident)==0){
            printf("That Queue Id is already taken\n");
            taken=1;
        }
    }
    if(taken==0){
    strcpy(mq.queue[qCounter].id,ident);
    printf("The id inside the method is %s and the qCounter is %d\n", mq.queue[qCounter].id, qCounter);
    qCounter++;
    }
}


